 # Image Base
 FROM node:latest

 # Directorio de la app en el  contenedor
WORKDIR /app

# Copia de archivos
ADD . /app

# Denpendencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm", "start"]
